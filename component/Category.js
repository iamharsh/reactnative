import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'

export default class Category extends Component {
    render() {
        return (
            <View style={styles.main}>
                <View style={{flex:2, marginTop: 10}}>
                    <Image style={{flex: 1, width: null, height: null, resizeMode: 'contain'}} source={this.props.imageUri}/>
                </View>
                <View style={{flex:1}}>
                    <Text style={{textAlign: 'center'}}>{this.props.name}</Text>
                </View>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    main: {
        height: 130, 
        width: 130, 
        marginLeft: 20, 
        borderRadius: 10, 
        borderWidth: 0.5, 
        borderColor: '#ddd'        
    }
})
