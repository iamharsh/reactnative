import React from 'react'
import { StyleSheet, Text, View, Button, Image, Dimensions, FlatList } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const data = [
    { key: "1", icon: <MaterialCommunityIcons name="apple-finder" size={40} color="#051937"></MaterialCommunityIcons>, image: <Image style={{width: 60, height: 60}} source={require('../assets/images/protein1.png')}/>, name: "Protein"},
    { key: "2", icon: <MaterialCommunityIcons name="badminton" size={40} color="#051937"></MaterialCommunityIcons>, image: <Image style={{width: 40, height: 40}} source={require('../assets/images/barbell.png')}/>, name: "Training"},
    { key: "3", icon: <MaterialCommunityIcons name="bell-ring" size={40} color="#051937"></MaterialCommunityIcons>, image: <Image style={{width: 40, height: 40}} source={require('../assets/images/weightlifting.png')}/>, name: "Exercise"},
    { key: "4", icon: <MaterialCommunityIcons name="biohazard" size={40} color="#051937"></MaterialCommunityIcons>, image: <Image style={{width: 40, height: 40}} source={require('../assets/images/heart.png')}/>, name: "Health"},
    { key: "5", icon: <MaterialCommunityIcons name="book" size={40} color="#051937"></MaterialCommunityIcons>},
    { key: "6", icon: <MaterialCommunityIcons name="chat-processing" size={40} color="#051937"></MaterialCommunityIcons>},
    { key: "7", icon: <MaterialCommunityIcons name="cloud-search" size={40} color="#051937"></MaterialCommunityIcons>},
    { key: "8", icon: <MaterialCommunityIcons name="discord" size={40} color="#051937"></MaterialCommunityIcons>},
    { key: "9", icon: <MaterialCommunityIcons name="glass-cocktail" size={40} color="#051937"></MaterialCommunityIcons>},
    // { key: "10", image: <Image style={{width: 40, height: 40}} source={require('../assets/images/protein1.png')}/>},
    // { key: "11", image: <Image style={{width: 40, height: 40}} source={require('../assets/images/barbell.png')}/>},
    // { key: "12", image: <Image style={{width: 40, height: 40}} source={require('../assets/images/weightlifting.png')}/>},
    // { key: "12", image: <Image style={{width: 40, height: 40}} source={require('../assets/images/heart.png')}/>},
];

const numColomns = 2;
class HomeScreen extends React.Component {    
    static navigationOptions = {
        title: 'Details',
        headerStyle: {
          backgroundColor: '#051937',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
    };

    renderItem = ({ item, index }) => {
        return (
            <View key={index} style={styles.item}>
                <View>{item.image}</View>
                <Text>{item.name}</Text>                
            </View>
        );
    };

    render() {
        return (
           <FlatList
            showsVerticalScrollIndicator={false}
            data={data}
            style={styles.container}
            renderItem={this.renderItem}
            numColumns={numColomns} 
           />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 10
    },
    item: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 5,
        height: 100,        
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    txt: {
        fontSize: 30,
        fontWeight: '700',
        color: '#fff'
    }
})

export default HomeScreen;