import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; 

const MenuIcon = () => <View style={{paddingLeft:16}}>
                            <Ionicons 
                            name="md-menu" 
                            size={32} 
                            color="#000" 
                            onPress={() => alert('Harsh')}/>
                        </View>
const PlaneIcon = () => <View style={{paddingRight:16}}>
                            <Ionicons 
                            name="ios-paper-plane" 
                            size={32} 
                            color="#2A9A71" 
                            onPress={() => alert('Harsh')}/>
                        </View>                        

class DetailsScreen extends React.Component {
    static navigationOptions = {        
        title: 'Dashboard',
        headerLeft: MenuIcon(alert),
        headerRight: PlaneIcon()          
      };
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Details Screen</Text>
                <Button
                    style={styles.btn}
                    title="PRESS HERE"
                    onPress={() => this.props.navigation.navigate('Welcome')}
                />                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    icon: {
      width: 24,
      height: 24,
    },
  });
  
  

export default DetailsScreen;  