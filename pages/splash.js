import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native';

class Splash extends React.Component {
    static navigationOptions = {
        header: null,              
    };

    componentWillMount(){
        setTimeout(() => {
            this.props.navigation.navigate('Welcome');
        }, 4000);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image style={styles.logo} 
                        source={require('../assets/icon.png')}>

                    </Image>
                </View>                
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#33333D'              
    },
    logoContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: "100%" 
    },
    logo: {
        width: 300,
        height: 165
    }         
}) 

export default Splash;  