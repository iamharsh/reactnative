import React, { Component } from 'react'
import { Text, View, SafeAreaView, StyleSheet, Platform, StatusBar, Image, Dimensions } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import Category from '../component/Category';

const {height,width} = Dimensions.get('window');
export class Feed extends Component {
    
    render() {
        return (
            <SafeAreaView style={{flex: 1, marginVertical: 12}}>
                <View style={{flex: 1}}>
                    <View style={{height: 65, backgroundColor: 'white', borderBottomWidth: 1, borderBottomColor: '#eee'}}>
                        <View style={{ flexDirection: 'row', 
                                        padding: 10, 
                                        backgroundColor: 'white', 
                                        marginHorizontal: 20, 
                                        shadowOffset: {width: 0, height: 0},
                                        shadowColor: 'black',
                                        shadowOpacity: 0.2,
                                        elevation: 1
                                    }}
                        >
                            <Ionicons name="ios-search" size={26}/>
                            <TextInput 
                                placeholder="Search here"
                                placeholderTextColor="grey"
                                style={{flex:1, fontWeight: '700', backgroundColor: 'white', marginLeft: 5}}
                            />
                        </View>
                    </View>
                    <ScrollView >
                        <View style={{flex:1, backgroundColor: '#fff', paddingTop: 15}}>
                            <Text style={{fontSize: 24, fontWeight: '700', paddingHorizontal: 20}}>
                                How can we help you to find best protein ?
                            </Text>
                            <View style={{height: 130, marginTop: 20}}>
                                <ScrollView 
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                >
                                    <Category name={'Protein'} imageUri={{uri: 'https://rukminim1.flixcart.com/image/612/612/joq2qa80/protein-supplement/u/k/9/on0230-optimum-nutrition-original-imafajgqhugbaf54.jpeg'}}/>
                                    <Category name={'Whey'} imageUri={{uri: 'https://rukminim1.flixcart.com/image/612/612/k0tw13k0/protein-supplement/y/h/c/100-powder-748927054637-optimum-nutrition-original-imafkeyftdpzqf6e.jpeg'}}/>
                                    <Category name={'Dymatize'} imageUri={{uri: 'https://rukminim1.flixcart.com/image/612/612/jp8ngcw0-1/protein-supplement/u/q/7/elite-whey-5-lbs-dym0003-dymatize-original-imafbj9psfdmhgy6.jpeg'}}/>
                                    <Category name={'BigMuscle'} imageUri={{uri: 'https://rukminim1.flixcart.com/image/612/612/jqcns7k0/protein-supplement/g/h/f/lean-gain-bm006-bigmuscles-nutrition-original-imafc9m3pfk7kzxb.jpeg'}}/>
                                </ScrollView>
                            </View>
                            <View style={{marginTop: 10, paddingHorizontal: 20}}>
                                <Text 
                                    style={{fontSize: 24, fontWeight: '700'}}>
                                        Introducing Its All About Journey
                                </Text>
                                <Text style={{fontWeight: '100', marginTop: 10}}>
                                    A complete destination where you can make your self muscular and healthier
                                </Text>
                                <View style={{width: width-40, height: 200, marginTop: 20}}>
                                    <Image
                                        style={{flex: 1, height: null, width: null, resizeMode: 'contain', borderRadius: 5, borderWidth: 1, borderColor: '#ddd'}}
                                        source={{uri: 'https://images.pexels.com/photos/1554824/pexels-photo-1554824.jpeg'}}/>
                                </View>
                            </View>
                        </View>
                        <View style={{marginTop: 10}}>
                            <Text style={{fontSize: 24, fontWeight: '700', paddingHorizontal: 20}}>
                                Workout for begginers        
                            </Text>     
                            <View style={{paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                                <View style={{width: width/2-30, height: width/2-30, borderWidth: 0.5, borderColor: '#ddd'}}>
                                    <View style={{flex: 1}}>
                                        <Image 
                                        style={{flex:1, width: null, height: null, resizeMode: 'cover'}}
                                        source={{uri: 'https://images.pexels.com/photos/841130/pexels-photo-841130.jpeg'}}/>
                                    </View>
                                    <View style={{flex:1, alignItems:'flex-start',justifyContent:'space-evenly',paddingLeft: 10}}>
                                        <Text style={{fontSize:10,color:'#b63838'}}>Dead Lift - 6 reps</Text>
                                        <Text style={{fontSize:12,fontWeight:'bold'}}>12 Days</Text>
                                        <Text style={{fontSize:10}}>1 hour</Text>
                                    </View>
                                </View>
                                <View style={{width: width/2-30, height: width/2-30, borderWidth: 0.5, borderColor: '#ddd'}}>
                                    <View style={{flex: 1}}>
                                        <Image 
                                        style={{flex:1, width: null, height: null, resizeMode: 'cover'}}
                                        source={{uri: 'https://images.pexels.com/photos/841130/pexels-photo-841130.jpeg'}}/>
                                    </View>
                                    <View style={{flex:1, alignItems:'flex-start',justifyContent:'space-evenly',paddingLeft: 10}}>
                                        <Text style={{fontSize:10,color:'#b63838'}}>Dead Lift - 6 reps</Text>
                                        <Text style={{fontSize:12,fontWeight:'bold'}}>12 Days</Text>
                                        <Text style={{fontSize:10}}>1 hour</Text>
                                    </View>
                                </View>        
                            </View>       
                        </View>      
                    </ScrollView>                    
                </View>
            </SafeAreaView>
        )
    }
}

export default Feed
