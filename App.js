import React from 'react';
import { createAppContainer } from 'react-navigation';

import Navigation from './navigation/index'

const AppContainer = createAppContainer(Navigation);

class App extends React.Component {
	render() {
		return (
			<AppContainer />
		);
	}
}
export default App





