import React, { Component } from 'react';
import { Image } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createSwitchNavigator } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';

import Splash from '../pages/splash';
import HomeScreen from '../pages/HomeScreen';
import DetailsScreen from '../pages/DetailsScreen';
import Feed from '../pages/Feed';
import Settings from '../pages/Settings';
import Profile from '../pages/Profile';

const Tab = createBottomTabNavigator(
    {   
        Welcome: {
            screen: HomeScreen,
            navigationOptions: {
                tabBarLabel: "Home",
                tabBarIcon: ({ tintColor }) => (
                   <Image source={require('../assets/images/home.png')}
                   style={{ height: 32, width: 32, }}/> 
                )
            },
        },     
        Feed: {
            screen: Feed,
            navigationOptions: {
                tabBarLabel: "Protein",
                tabBarIcon: ({ tintColor }) => (
                    <Image source={require('../assets/images/protein1.png')}
                   style={{ height: 32, width: 32, }}/>
                )
            },
        },
        Settings: {
            screen: Settings,
            navigationOptions: {
                tabBarLabel: "Training",
                tabBarIcon: ({ tintColor }) => (
                    <Image source={require('../assets/images/barbell.png')}
                   style={{ height: 32, width: 32, }}/>
                )
            },
        },
        
        Profile: {
            screen: Profile,
            navigationOptions: {
                tabBarLabel: "Exercise",
                tabBarIcon: ({ tintColor }) => (
                    <Image source={require('../assets/images/weightlifting.png')}
                   style={{ height: 32, width: 32, }}/>
                )
            },
        },
        Details: {
            screen: DetailsScreen,
            navigationOptions: {
                tabBarLabel: "Health",
                tabBarIcon: ({ tintColor }) => (
                    <Image source={require('../assets/images/heart.png')}
                   style={{ height: 32, width: 32, }}/>
                )
            },
        },
    },
    
    {
        navigationOptions: ({ navigation }) => {
            const { routeName } = navigation.state.routes[navigation.state.index];
            return {
                headerTitle: routeName,
            };            
        },
        tabBarOptions: {
            activeTintColor: '#e91e63',
            labelStyle: {
              fontSize: 12,
              fontWeight: '600'
            },
            style: {
              backgroundColor: '#fff',
              borderTopWidth: 0,              
              height: 65,
              shadowColor: "#000",
              shadowOffset: { width: 1, height: 0, },
              shadowOpacity: 1.8,
              elevation: 3,            
            },
        },
    }
)

const DashboardNavigator = createStackNavigator({
    TabNavigator: Tab
}, {
    defaultNavigationOptions: ({ navigation }) => {
        return {
            headerLeft: (
                <Ionicons
                    style={{ paddingLeft: 16 }}
                    name="md-menu"
                    size={32}
                    color="#000"
                    onPress={() => navigation.openDrawer()} />
            ),
            headerRight: (
                <Ionicons
                    style={{ paddingRight: 16 }}
                    name="ios-paper-plane"
                    size={32}
                    color="#000"
                    onPress={() => alert('Harsh')} />
            )
        }
    }
});

const Drawer = createDrawerNavigator(
    {
        Welcome: {
            screen: DashboardNavigator,
            navigationOptions: {
                drawerLabel: "Dashboard",
                drawerIcon: ({ tintColor }) => (
                    <Ionicons name="ios-keypad" size={28} />
                )
            },
        },
        Feed: {
            screen: Feed,
            navigationOptions: {
                drawerLabel: "Feed",
                drawerIcon: ({ tintColor }) => (
                    <Ionicons name="ios-filing" size={28} />
                )
            },
        },
        Settings: {
            screen: Settings,
            navigationOptions: {
                drawerLabel: "Settings",
                drawerIcon: ({ tintColor }) => (
                    <Ionicons name="ios-construct" size={28} />
                )
            },
        },
        Profile: {
            screen: Profile,
            navigationOptions: {
                drawerLabel: "Profile",
                drawerIcon: ({ tintColor }) => (
                    <Ionicons name="ios-contact" size={28} />
                )
            },
        },
        Details: {
            screen: DetailsScreen,
            navigationOptions: {
                drawerLabel: "Details",
                drawerIcon: ({ tintColor }) => (
                    <Ionicons name="ios-bookmarks" size={28} />
                )
            },
        },        
    }
)

const Navigation = createSwitchNavigator(
    {
        Welcome: { screen: Drawer },
        Details: { screen: DetailsScreen },
        Splash: { screen: Splash },
        Feed: { screen: Feed }
    },
    {
        initialRouteName: 'Splash',
    },

);

export default Navigation;
